#!/usr/bin/env python
# coding: utf-8

# In[6]:


# script Qualtics API Script 7/2/
# Steinhardt has 28 Qualtrics report 
# Reports need to refreshed per day
# Plan from data frame writes to a csv: overwrites
# import py_qualtrics_api.tools as pq --- not loading,works in pycharm works
# https://docs.python.org/3/howto/logging.html


import logging                as log
import pandas                 as pd
import sys                    as sys
import requests               as requests
import json                   as json
import csv                    as csv
import os                     as os
import zipfile                as zipfile 
import smtplib                as smtplib
import io                     as io

from   datetime               import datetime
from   requests.structures    import CaseInsensitiveDict

print(os.getcwd())

# email sent
import smtplib
from   email.mime.multipart   import MIMEMultipart
from   email.mime.text        import MIMEText



# In[7]:


# FedericoTartarini/import_qualtrics.py
# https://www.youtube.com/watch?v=_uhY_a4NgNc
# https://gist.github.com/FedericoTartarini/9496282b4b2f508c0ab2da96f4955397

def get_qualtrics_survey(survey_id,survey_name):    
    print('-----------------------')
    print('Survey Id: ',survey_id,' Survey Name: ',survey_name,' date time:',datetime.now())
    api_token       = "26KLg3CqvVtWD5c5ZZmpFxGA2A8t6r2Tb1kluYHt"
    file_format     = "csv"
    data_center     = 'nyta.sjc1' # "<Organization ID>.<Datacenter ID>"
    survey_id       =  survey_id
    name            =  survey_name
    
    
    dir_save_survey = '//Users//jps433//Qualtrics API//Log'

# Setting static parameters
    request_check_progress = 0
    progress_status = "in progress"
    #base_url = "https://{0}.qualtrics.com/API/v3/responseexports/".format(data_center)
    base_url = "https://{0}.qualtrics.com/API/v3/responseexports/".format(data_center)
    headers = {
        "content-type": "application/json",
        "x-api-token": api_token,
    }
    nowStart = datetime.now()
    # Step 1: Creating Data Export
    download_request_url = base_url
    download_request_payload = '{"format":"' + file_format + '","surveyId":"' + survey_id + '"}'
    download_request_response = requests.request("POST", download_request_url,
                                                 data=download_request_payload,
                                                 headers=headers)
    progress_id = download_request_response.json()["result"]["id"]
    # Step 2: Checking on Data Export Progress and waiting until export is ready
    while request_check_progress < 100 and progress_status != "complete":
        request_check_url = base_url + progress_id
        request_check_response = requests.request("GET", request_check_url, headers=headers)
        request_check_progress = request_check_response.json()["result"]["percentComplete"]
    request_download_url = base_url + progress_id + '/file'
    request_download = requests.request("GET", request_download_url, headers=headers, stream=True)   
# Step 4: Unzipping the file   
    zipfile.ZipFile(io.BytesIO(request_download.content)).extractall(dir_save_survey)
    nowEnd = datetime.now()
    duration = nowEnd - nowStart
    logging.info('Start time:',nowStart)
    print('End time:'  ,nowEnd)
    print('Duration:'  ,duration,' - seconds' )
    print('------------------')


# In[106]:


#test 
#get_qualtrics_survey('SV_6VVTN1KkL26usip' ,'Midterm Fieldwork Evaluation - Art Therapy - Active Since Fall 2019')


# In[8]:


df = pd.read_csv("Steinhardt Survey IDs for Qualtrics Webconnector - Sheet1.csv")
dfparameters=[["Survey ID"],["Survey Name"]]

import logging
df = pd.read_csv("Steinhardt Survey IDs for Qualtrics Webconnector - Sheet1.csv")
dfparameters=df[["Survey ID","Survey Name"]]
now = datetime.now() # current date and time
logfile='QualtricsLog-'+now.strftime("%m%d%Y-%Hh%Mm%Ss")
#logging.basicConfig(filename=logfile,encoding='utf-8',  level=logging.DEBUG)
handler = logging.FileHandler('test.log', 'w', 'utf-8') 
for index, row in dfparameters.iterrows():
    #print(row["Survey ID"], row["Survey Name"])
    get_qualtrics_survey(row["Survey ID"],row["Survey Name"])
    
    
    
             


# In[9]:


# email sent

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
mail_content = 'Qualtrics Surveys ETL refresh completed. Tableau Team'

# https://www.tutorialspoint.com/send-mail-from-your-gmail-account-using-python
#The mail addresses and password
#https://support.google.com/mail/answer/185833?hl=en
# At the bottom, choose Select app and choose the app you using and then Select device and choose the device you’re using and then Generate.
# Follow the instructions to enter the App Password. The App Password is the 16-character code in the yellow bar on your device.
# Tap Done.

# gmail 
sender_address     =  'petersidi@gmail.com'
sender_pass        =  '#E189psi8062-London'
receiver_address   =  'jps433@nyu.edu'
#Setup the MIME
message = MIMEMultipart()
message['From']    = sender_address
message['To']      = receiver_address
message['Subject'] = 'Qualtrics Survey ETL'   #The subject line
#The body and the attachments for the mail
message.attach(MIMEText(mail_content, 'plain'))
#Create SMTP session for sending the mail
session = smtplib.SMTP('smtp.gmail.com', 587) #use gmail with port
session.starttls() #enable security
session.login(sender_address, sender_pass) #login with mail_id and password
text = message.as_string()
session.sendmail(sender_address, receiver_address, text)
session.quit()
print('Mail Sent')


# In[ ]:




